#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QObject>
#include <iostream>
#include <fstream>
#include <QDateTime>
#include "networkdaemon.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication *app = new QCoreApplication(argc, (char**)argv);
    Networkdaemon networkConn;
    bool isOnline = false;
    bool isZeroData = false;
    QVariant prevBytes = 0;
    QVariant prevTodayBytes = 0;
    QVariant totBytes = 0;
    QVariant connectionType;
    QVariant tLastPos = 0;
    QVariant todayMBytes = 0;
    QVariant totalTodayMBytes = 0;
    QVariant WFtotData = 0;
    QVariant SIMtotData = 0;
    QVariant todDataNum = 0;
    QList<QVariant> whichConn;
    QString saveLastPos;
    QString saveBytes;
    QString readData;
    QVariant nowIsTimeAndDate;
    string saveLastPosStr;
    bool isRebooted = false;
    int todayPosition;
    string todayIsUnity8;
    string saveBytesStr;
    string todDataStr;
    int numConns;
    double todayMbytesDouble = 0;
    double lastRebootMbyte = 0;
    double totBytesDbl = 0;
    fstream tBytesFile;
    fstream todayPosFile;
    fstream todayPosFileSIM;
    fstream tBytesFileSIM;
    fstream tBytesSaveFile;
    fstream tBytesSaveFileSIM;

    fstream rebootedFile;
    rebootedFile.open(".config/datamonitor.matteobellei/rebooted.conf",ios::in);
    rebootedFile>>todayIsUnity8;
    QString todayIsUnity8Str = QString::fromStdString(todayIsUnity8);
    rebootedFile.close();
    nowIsTimeAndDate = QDateTime::currentDateTime();
    qDebug() << "|-----------Date and time" << "-----------|";
    qDebug() << "|      " << nowIsTimeAndDate.toString() << "       |";
    qDebug() << "|------------------------------------|";
    QQmlEngine engine;
    engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
    QQmlComponent component(&engine,
        QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/Daemon.qml")));
    if (component.isError()) {
        qWarning() << component.errors();
    }
    QObject *object = component.create();
    if (todayIsUnity8Str=="unity8") {
        todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
        if(!todayPosFile) {
          qDebug()<<"Error in reading todayPos.conf file..";
          todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);  // database for Wi-fi to keep track of the last saved data, according to last position in today data database
          todayPosFile<<"0";
          qDebug()<<"File .config/datamonitor.matteobellei/todayPos.conf created.";
        }
        todayPosFile.close();
        todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",ios::in);
        if(!todayPosFileSIM) {
          qDebug()<<"Error in reading todayPosSIM.conf file..";
          todayPosFile.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);  // line needed to create the file for the first time
          todayPosFileSIM<<"0";
          qDebug()<<"File .config/datamonitor.matteobellei/todayPosSIM.conf created.";
        }
        todayPosFileSIM.close();
        tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
        if(!tBytesFile) {
          qDebug()<<"Error in reading todayBytes.conf file..";
          tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",fstream::out);  // line needed to create the file for the first time
          tBytesFile<<"0";
          qDebug()<<"File .config/datamonitor.matteobellei/todayBytes.conf created";
        }
        tBytesFile.close();
        tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",ios::in);
        if(!tBytesFileSIM) {
          qDebug()<<"Error in reading todayBytesSIM.conf file..";
          tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",fstream::out);  // line needed to create the file for the first time
          tBytesFileSIM<<"0";
          qDebug()<<"File .config/datamonitor.matteobellei/todayBytesSIM.conf created";
        }
        tBytesFileSIM.close();
        tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",ios::in);
        if(!tBytesSaveFile) {
          qDebug()<<"Error in reading tBytesSave.conf file..";
          tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);  // line needed to create the file for the first time
          tBytesSaveFile<<"0";
          qDebug()<<"File .config/datamonitor.matteobellei/tBytesSave.conf created";
        }
        tBytesSaveFile.close();
        tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",ios::in);
        if(!tBytesSaveFileSIM) {
          qDebug()<<"Error in reading tBytesSaveSIM.conf file..";
          tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);  // line needed to create the file for the first time
          tBytesSaveFileSIM<<todDataStr;
          qDebug()<<"File .config/datamonitor.matteobellei/tBytesSaveSIM.conf created";
        }
        tBytesSaveFileSIM.close();
        for (int i=0; i <= 1; ++i) {
                 if (i==0) {
                   connectionType="WIFI";
                 } else {
                   connectionType="SIM";
                 }
                 QMetaObject::invokeMethod(object, "todayLastPos",
                                   Q_RETURN_ARG(QVariant, tLastPos),
                                   Q_ARG(QVariant, connectionType));
                 saveLastPos = tLastPos.toString();
                 saveLastPosStr = saveLastPos.toUtf8().constData();
                 QMetaObject::invokeMethod(object, "createDatabase",
                                   Q_ARG(QVariant, connectionType));
                 QMetaObject::invokeMethod(object, "fillVacantDays",
                                   Q_RETURN_ARG(QVariant, prevBytes),
                                   Q_ARG(QVariant, connectionType));
                 if (prevBytes.isNull()) {
                    prevBytes=0;
                 }
                 qDebug()<< "Yesterday" << connectionType.toString() << "stored bytes:" << prevBytes.toDouble() << "MB";
                 if (i==0) {
                    todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);
                    todayPosFile<<saveLastPosStr;
                    todayPosFile.close();
                    WFtotData = prevBytes;
                    tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",fstream::out);
                    tBytesFile<<"0";
                    tBytesFile.close();
                    tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);
                    tBytesSaveFile<<"0";
                    tBytesSaveFile.close();
                  } else {
                    todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);
                    todayPosFileSIM<<saveLastPosStr;
                    todayPosFileSIM.close();
                    SIMtotData = prevBytes;
                    tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",fstream::out);
                    tBytesFileSIM<<"0";
                    tBytesFileSIM.close();
                    tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);
                    tBytesSaveFileSIM<<"0";
                    tBytesSaveFileSIM.close();
                  }
                  if (tLastPos!=0) {
                    isRebooted = true;
                  }
        }
        if (isRebooted == true) {
            qDebug() << "Phone re-booted today.";
        } else {
            qDebug() << "Phone first start-up: no data stored for today yet.";
            networkConn.trigIndicator(WFtotData.toDouble(), SIMtotData.toDouble());
        }
        rebootedFile.open(".config/datamonitor.matteobellei/rebooted.conf",fstream::out);
        rebootedFile<<"mattdaemon-service";
        rebootedFile.close();
    } else if (todayIsUnity8Str=="mattdaemon-service") {
         whichConn = networkConn.recBytes();
         int i=0;
         numConns = whichConn.size();
         while (i < numConns) {
                if (whichConn[i]=="WIFI") {
                  todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",ios::in);
                  todayPosFile>>todayPosition;
                  todayPosFile.close();
                } else if (whichConn[i]=="SIM") {
                  todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",ios::in);
                  todayPosFileSIM>>todayPosition;
                  todayPosFileSIM.close();
                }
                QMetaObject::invokeMethod(object, "yesterdayData",
                                Q_RETURN_ARG(QVariant, prevBytes),
                                Q_ARG(QVariant, whichConn[i]));
                if (prevBytes.isNull()) {
                   prevBytes=0;
                }
                prevTodayBytes = prevBytes;
                QMetaObject::invokeMethod(object, "todayLastPos",
                                  Q_RETURN_ARG(QVariant, tLastPos),
                                  Q_ARG(QVariant, whichConn[i]));
                if (todayPosition>0) {
                  if (tLastPos!=0) {
                      QVariant todayPositionBack = todayPosition;
                      QMetaObject::invokeMethod(object, "todayPrevBytes",
                                    Q_RETURN_ARG(QVariant, prevTodayBytes),
                                    Q_ARG(QVariant, todayPositionBack),
                                    Q_ARG(QVariant, whichConn[i]));
                      qDebug()<< "Today earlier stored bytes for" << whichConn[i].toString() << "connection is:" << prevTodayBytes.toDouble() << "MB";
                  } else {
                      todayPosition = 0;
                      if (whichConn[i]=="WIFI") {
                        todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);
                        todayPosFile<<todayPosition;
                        todayPosFile.close();
                        tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
                        tBytesFile>>todDataStr;
                        tBytesFile.close();
                        tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);
                        tBytesSaveFile<<todDataStr;
                        tBytesSaveFile.close();
                      } else if (whichConn[i]=="SIM") {
                        todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);
                        todayPosFileSIM<<todayPosition;
                        todayPosFileSIM.close();
                        tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",ios::in);
                        tBytesFileSIM>>todDataStr;
                        tBytesFileSIM.close();
                        tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);
                        tBytesSaveFileSIM<<todDataStr;
                        tBytesSaveFileSIM.close();
                      }
                  }
                } else {
                  if (tLastPos==0) {
                    qDebug()<< "Today there are no stored bytes yet for" << whichConn[i].toString() << "connection (minimum size to be stored is 0.1 MB)";
                    if (whichConn[i]=="WIFI") {
                      tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
                      tBytesFile>>todDataStr;
                      tBytesFile.close();
                      tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);
                      tBytesSaveFile<<todDataStr;
                      tBytesSaveFile.close();
                    } else if (whichConn[i]=="SIM") {
                      tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",ios::in);
                      tBytesFileSIM>>todDataStr;
                      tBytesFileSIM.close();
                      tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);
                      tBytesSaveFileSIM<<todDataStr;
                      tBytesSaveFileSIM.close();
                    }
                  }
                }
                if (prevBytes==0 & tLastPos==0) {
                    QMetaObject::invokeMethod(object, "fillVacantDays",
                                      Q_RETURN_ARG(QVariant, prevBytes),
                                      Q_ARG(QVariant, whichConn[i]));
                    prevTodayBytes = prevBytes;
                    if (todayPosition==0) {
                       networkConn.resetNotification(whichConn[i]);
                       qDebug()<< "All fired notifications have been reset for the current month.";
                       if (whichConn[i]=="WIFI") {
                          tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);
                          tBytesSaveFile<<"0";
                          tBytesSaveFile.close();
                        } else if (whichConn[i]=="SIM") {
                          tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);
                          tBytesSaveFileSIM<<"0";
                          tBytesSaveFileSIM.close();
                        }
                    }
                }
                if (whichConn[i]=="WIFI") {
                    tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",ios::in);
                    tBytesSaveFile>>todDataStr;
                    readData = QString::fromStdString(todDataStr);
                    todDataNum = readData.toDouble();
                    tBytesSaveFile.close();
                } else if (whichConn[i]=="SIM") {
                    tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",ios::in);
                    tBytesSaveFileSIM>>todDataStr;
                    readData = QString::fromStdString(todDataStr);
                    todDataNum = readData.toDouble();
                    tBytesSaveFileSIM.close();
                }
                todayMbytesDouble = (whichConn[i+1].toDouble()-todDataNum.toDouble())/1048576;
                todayMBytes = todayMbytesDouble;
                QMetaObject::invokeMethod(object, "storeNewData",
                              Q_RETURN_ARG(QVariant, totBytes),
                              Q_ARG(QVariant, todayMBytes),
                              Q_ARG(QVariant, prevTodayBytes),
                              Q_ARG(QVariant, prevBytes),
                              Q_ARG(QVariant, whichConn[i]));
                if (totBytes>0) {
                    if (whichConn[i]=="WIFI") {
                       saveBytes = whichConn[i+1].toString();
                       saveBytesStr = saveBytes.toUtf8().constData();
                       tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",fstream::out);
                       tBytesFile<<saveBytesStr;
                       tBytesFile.close();
                    } else if (whichConn[i]=="SIM") {
                       saveBytes = whichConn[i+1].toString();
                       saveBytesStr = saveBytes.toUtf8().constData();
                       tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",fstream::out);
                       tBytesFileSIM<<saveBytesStr;
                       tBytesFileSIM.close();
                    }
                } else {
                  totBytesDbl = -totBytes.toDouble();
                  totBytes = totBytesDbl;
                }
                totalTodayMBytes = totBytes.toDouble()-prevBytes.toDouble();
                lastRebootMbyte = whichConn[i+1].toDouble()/1048576;
                qDebug()<< "Today received Mbytes for" << whichConn[i].toString() << "connection after last device reboot is:" << lastRebootMbyte << "MB";
                qDebug()<< "Today received Mbytes for" << whichConn[i].toString() << "connection is:" << totalTodayMBytes.toDouble() << "MB";
                qDebug()<< "Total received Mbytes for" << whichConn[i].toString() << "connection is:" << totBytes.toDouble() << "MB";
                networkConn.shootNotification(whichConn[i], totBytes);
                if (totalTodayMBytes.toDouble()>=0) {
                   networkConn.saveDataIndicator(whichConn[i].toString(), totalTodayMBytes.toDouble(), totBytes.toDouble());
                }
                i = i+2;
         }
         if (numConns==0) {
           qDebug()<< "Network connections not available.";
           for (int i=0; i <= 1; ++i) {
                if (i==0) {
                  connectionType="WIFI";
                } else {
                  connectionType="SIM";
                }
                QMetaObject::invokeMethod(object, "todayLastPos",
                                  Q_RETURN_ARG(QVariant, tLastPos),
                                  Q_ARG(QVariant, connectionType));
                if (tLastPos==0) {
                      QMetaObject::invokeMethod(object, "createDatabase",
                                        Q_ARG(QVariant, connectionType));
                      QMetaObject::invokeMethod(object, "fillVacantDays",
                                        Q_RETURN_ARG(QVariant, prevBytes),
                                        Q_ARG(QVariant, connectionType));
                      if (prevBytes.isNull()) {
                         prevBytes=0;
                      }
                      if (connectionType=="WIFI") {
                         todayPosFile.open(".config/datamonitor.matteobellei/todayPos.conf",fstream::out);
                         todayPosFile<<0;
                         todayPosFile.close();
                         tBytesFile.open(".config/datamonitor.matteobellei/todayBytes.conf",ios::in);
                         tBytesFile>>todDataStr;
                         tBytesFile.close();
                         tBytesSaveFile.open(".config/datamonitor.matteobellei/tBytesSave.conf",fstream::out);
                         tBytesSaveFile<<todDataStr;
                         tBytesSaveFile.close();
                      } else if (connectionType=="SIM") {
                         todayPosFileSIM.open(".config/datamonitor.matteobellei/todayPosSIM.conf",fstream::out);
                         todayPosFileSIM<<0;
                         todayPosFileSIM.close();
                         tBytesFileSIM.open(".config/datamonitor.matteobellei/todayBytesSIM.conf",ios::in);
                         tBytesFileSIM>>todDataStr;
                         tBytesFileSIM.close();
                         tBytesSaveFileSIM.open(".config/datamonitor.matteobellei/tBytesSaveSIM.conf",fstream::out);
                         tBytesSaveFileSIM<<todDataStr;
                         tBytesSaveFileSIM.close();
                      }
                      networkConn.saveDataIndicator(connectionType.toString(), tLastPos.toDouble(), prevBytes.toDouble());
                }
           }
         }
    }
    delete object;
    return 0;
}
