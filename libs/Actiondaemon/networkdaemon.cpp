#include <stdint.h>
#include <QNetworkSession>
#include <QNetworkConfigurationManager>
#include <fstream>
#include <iostream>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>

#include "networkdaemon.h"

using namespace std;

QList<QVariant> Networkdaemon::recBytes() {
  quint64 bytes;
  QList<QVariant> connBytes;
  QNetworkConfigurationManager manager;
  manager.updateConfigurations();
  int count=0;
  QList<QNetworkConfiguration> activeConfigs = manager.allConfigurations(QNetworkConfiguration::Active);
  const bool Datastat = (manager.capabilities() & QNetworkConfigurationManager::DataStatistics);
  if (activeConfigs.count()>0) {
    if (Datastat) {
      qDebug() << "Platform is capable of data statistics for available connections.";
    }  else {
      qDebug() << "Platform is not capable of data statistics for available connections.";
    }
  }
int j = 0;
for (int i=0; i < activeConfigs.count(); ++i) {
      qDebug() << activeConfigs.value(i).name();
      qDebug() << activeConfigs.value(i).bearerTypeName();
      qDebug() << activeConfigs.value(i).identifier();
      if (activeConfigs.value(i).bearerTypeFamily()==3 || activeConfigs.value(i).bearerTypeFamily()==11 || activeConfigs.value(i).bearerTypeFamily()==12) {
        connBytes << "SIM";
      } else {
        if (activeConfigs.value(i).bearerTypeFamily()==2) {
          connBytes << "WIFI";
        }
      }
      QNetworkConfiguration cfg = manager.configurationFromIdentifier(activeConfigs.value(i).identifier());
      QNetworkSession *session = new QNetworkSession(cfg);
      if (session->isOpen()==false) {
        session->open();
        session->waitForOpened(1000);
      } else {
        qDebug() << "Session is open!";
      }
      if (session->isOpen()) {
        qDebug() << "Session is open!";
        if (session->state()==session->Connected) {
          qDebug() << "Session connected!";
        } else {
            if (session->state()==session->NotAvailable) {
              qDebug() << "Session not available.";
            } else {
              if (session->state()==session->Connecting) {
                qDebug() << "Session connecting...";
              } else {
                if (session->state()==session->Invalid) {
                  qDebug() << "Session not valid.";
                } else {
                  if (session->state()==session->Closing) {
                    qDebug() << "Session closing...";
                  } else {
                    if (session->state()==session->Disconnected) {
                      qDebug() << "Session disconnected.";
                    } else {
                      if (session->state()==session->Roaming) {
                        qDebug() << "Session is roaming...";
                      } else {
                        qDebug() << "Session state not identified...";
                      }
                    }
                  }
                }
              }
          }
        }
      } else {
        qDebug() << "Session is still closed.";
      }
      if (connBytes[j]=="WIFI") {
        bytes = session->bytesReceived();
        connBytes << bytes;
        j= j+2;
      } else if (connBytes[j]=="SIM") {
        bytes = Networkdaemon::recBytesFromCMD();
        connBytes << bytes;
        j= j+2;
      }
}
  return connBytes;
}

quint64 Networkdaemon::recBytesFromCMD() {
  fstream rxFile;
  string address = "/sys/class/net/";
  string deviceName_text;
  double sumSIMdata = 0;
  quint64 receivedBytesBack = 0;
  bool isUp=false;
  bool isUpbis=false;
  bool isRunning=false;
  bool isRunningbis=false;
  QString message;
  QVariant deviceName;
  QString deviceNameStr;
  QString receivedBytes;
  QList<QString> connsSIM;
  string receivedbytesStr;
  QNetworkInterface interfacesAll;
  QList<QNetworkInterface> interfacesList = interfacesAll.allInterfaces();
  if (interfacesList.count()>0) {
    int i=1;
    while (i <= interfacesList.count()) {
          deviceName = interfacesList.value(i).name();
          deviceNameStr = deviceName.toString();
          isUp = (interfacesList.value(i).flags() & QNetworkInterface::IsUp);
          if (isUp & deviceNameStr != "wlan0") {
            if (isUp) {
              isUpbis = true;
            }
            isRunning = (interfacesList.value(i).flags() & QNetworkInterface::IsRunning);
            if (isRunning) {
              if (isRunning) {
                isRunningbis = true;
              }
              const bool isPointToPoint = (interfacesList.value(i).flags() & QNetworkInterface::IsPointToPoint);
              if (isPointToPoint) {
                 qDebug() << "Interface is a Point to Point type.";
              }
              const bool canBroadcast = (interfacesList.value(i).flags() & QNetworkInterface::CanBroadcast);
              if (canBroadcast) {
                qDebug() << "Interface is able to broadcast.";
              }
              const bool isloopBack = (interfacesList.value(i).flags() & QNetworkInterface::IsLoopBack);
              if (isloopBack) {
                qDebug() << "Interface is a loopback type.";
              }
              const bool canMulticast = (interfacesList.value(i).flags() & QNetworkInterface::CanMulticast);
              if (canMulticast) {
                qDebug() << "Interface is able to multicast.";
              }
              qDebug() << "Interface for running SIM is:" << deviceNameStr;
              connsSIM << deviceNameStr;
            }
          }
          i = i+1;
    }
  } else {
    qDebug() << "No interfaces found for the SIM connection, therefore received data storage is not available.";
  }
  if (isUpbis) {
      if (!isRunningbis) {
         qDebug() << "At least one interface for the SIM connection is up but none is running, therefore received data monitoring and storage is not available.";
      }
  } else {
    qDebug() << "None of the interfaces for the SIM connection is up, therefore received data monitoring and storage is not available.";
  }
  if (connsSIM.count()==0) {
    return 0;
  } else {
    int j=1;
    while (j <= connsSIM.count()) {
        deviceName_text = connsSIM[j-1].toUtf8().constData();
        address += deviceName_text + "/statistics/rx_bytes";
        rxFile.open(address.c_str(),ios::in);
        rxFile>>receivedbytesStr;
        receivedBytes = QString::fromStdString(receivedbytesStr);
        sumSIMdata = sumSIMdata + receivedBytes.toDouble();
        rxFile.close();
        j=j+1;
    }
    receivedBytesBack = sumSIMdata;
    return receivedBytesBack;
  }
}

void Networkdaemon::shootNotification(const QVariant connType, const QVariant totBytes) {
  string tokenID;
  bool enableNotify;
  QString message;
  fstream tokenFile;
  fstream notifyFile;
  QVariant tokenDest;
  QVariant sizeDB = 0;
  QVariant dataBytes = 0;
  QVariant silenFlag = 0;
  QVariant notifFlag = 0;
  QList<QVariant> numThresholds;
  QList<QVariant> numThresholds_new;
  QList<QString> stringsReturn;
  QQmlEngine engine;
  engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
  QQmlComponent component(&engine,
      QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/ThresholdsHandler.qml")));
  if (component.isError()) {
      qWarning() << component.errors();
  }
  QObject *object = component.create();
  QMetaObject::invokeMethod(object, "getSize",
                    Q_RETURN_ARG(QVariant, sizeDB),
                    Q_ARG(QVariant, connType));
  for (int i=0; i < sizeDB.toInt(); ++i) {
      QMetaObject::invokeMethod(object, "getSilFlag",
                        Q_RETURN_ARG(QVariant, silenFlag),
                        Q_ARG(QVariant, i),
                        Q_ARG(QVariant, connType));
      if (silenFlag==0) {
          QMetaObject::invokeMethod(object, "getNotifFlag",
                            Q_RETURN_ARG(QVariant, notifFlag),
                            Q_ARG(QVariant, i),
                            Q_ARG(QVariant, connType));
          if (notifFlag==0) {
            QMetaObject::invokeMethod(object, "getValue",
                              Q_RETURN_ARG(QVariant, dataBytes),
                              Q_ARG(QVariant, i),
                              Q_ARG(QVariant, connType));
            if (dataBytes!=0 & dataBytes<=totBytes) {
                numThresholds << dataBytes;
                QMetaObject::invokeMethod(object, "updateNotifFlag",
                                  Q_ARG(QVariant, 1),
                                  Q_ARG(QVariant, dataBytes),
                                  Q_ARG(QVariant, connType));
            }
          }
      }
  }
  delete object;
  QVariant min_value = 0;
  QVariant max_value = 0;
  bool inside = false;
  int k = 0;
  if (numThresholds.size()>0) {
    if (numThresholds.size()==1) {
        numThresholds_new=numThresholds;
    } else {
        numThresholds_new<<k;
    }
    stringsReturn = Networkdaemon::traslationNotif();
  }
  if (numThresholds.size()>1) {   // Algorithm for ordering array from the smallest value to the biggest
        for (int j=0; j < numThresholds.size(); ++j) {
              if (numThresholds[j]>numThresholds_new[j]) {
                  min_value = numThresholds[j];
              } else {
                  min_value = 1000000000;
              }
              while (k < numThresholds.size()) {
                      if (numThresholds[k]>numThresholds_new[j]) {
                        if (numThresholds[k]< min_value) {
                          min_value=numThresholds[k];
                        } else {
                          if (numThresholds[k]>max_value) {
                            max_value=numThresholds[k];
                          }
                        }
                      }
                    k=k+1;
              }
              if (j<numThresholds.size()-1) {
                  numThresholds_new[j]=min_value;
                  numThresholds_new<<min_value;
              } else {
                numThresholds_new[j]=max_value;
              }
              k=0;
        }
  }
  for (int i=0; i < numThresholds_new.size(); ++i) {
    if (stringsReturn.size()==3) {
      message = stringsReturn[0] + connType.toString() + "- " + stringsReturn[1] + " " + QString::number(numThresholds_new[i].toDouble(), 'f', 0) + " MBytes " + stringsReturn[2];
    } else {
      message = "Data threshold for -" + connType.toString() + "- " + "set at" + " " + QString::number(numThresholds_new[i].toDouble(), 'f', 0) + " MBytes " + "was exceeded!";
    }
    qDebug() << message;
    string messageSTDString = message.toStdString();
        string command = "/usr/bin/gdbus call --session --dest com.ubuntu.Postal --object-path /com/ubuntu/Postal/datamonitor_2ematteobellei --method com.ubuntu.Postal.Post datamonitor.matteobellei_datamonitor ";
        command = command + "'\"{\\\"message\\\": \\\"foobar\\\", \\\"notification\\\":{\\\"card\\\": {\\\"icon\\\": \\\"notification\\\", \\\"summary\\\": \\\"dataMonitor\\\", \\\"body\\\": " + "\\\"" + messageSTDString + "\\\", \\\"popup\\\": true, \\\"persist\\\": true}, \\\"sound\\\": true, \\\"vibrate\\\": {\\\"pattern\\\": [200, 100], \\\"duration\\\": 200,\\\"repeat\\\": 2 }}}\"'";
        FILE* pipe = popen(command.c_str(), "r");
  }
}

void Networkdaemon::resetNotification(const QVariant connType) {
  QVariant sizeDB = 0;
  QVariant dataBytes = 0;
  QVariant notifFlag = 0;
  QQmlEngine engine;
  engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
  QQmlComponent component(&engine,
      QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/ThresholdsHandler.qml")));
  if (component.isError()) {
      qWarning() << component.errors();
  }
  QObject *object = component.create();
  QMetaObject::invokeMethod(object, "getSize",
                    Q_RETURN_ARG(QVariant, sizeDB),
                    Q_ARG(QVariant, connType));
  for (int i=0; i < sizeDB.toInt(); ++i) {
    QMetaObject::invokeMethod(object, "getNotifFlag",
                      Q_RETURN_ARG(QVariant, notifFlag),
                      Q_ARG(QVariant, i),
                      Q_ARG(QVariant, connType));
    if (notifFlag==1) {
      QMetaObject::invokeMethod(object, "getValue",
                        Q_RETURN_ARG(QVariant, dataBytes),
                        Q_ARG(QVariant, i),
                        Q_ARG(QVariant, connType));
      QMetaObject::invokeMethod(object, "updateNotifFlag",
                        Q_ARG(QVariant, 0),
                        Q_ARG(QVariant, dataBytes),
                        Q_ARG(QVariant, connType));
    }
  }
}

void Networkdaemon::saveDataIndicator(const QString whichConn, const double todayData, const double totalData) {
  QJsonObject object;
  QString tdyWF = "today_data_WIFI";
  QString ttlWF = "total_data_WIFI";
  QString tdySIM = "today_data_SIM";
  QString ttlSIM = "total_data_SIM";
  QVariant todayDataQV = roundNMB(todayData);
  QVariant totalDataQV = roundNMB(totalData);
  if (whichConn=="WIFI") {
    object.insert(tdyWF, QJsonValue(todayDataQV.toString()));
    object.insert(ttlWF, QJsonValue(totalDataQV.toString()));
  } else {
    if (whichConn=="SIM") {
    object.insert(tdySIM, QJsonValue(todayDataQV.toString()));
    object.insert(ttlSIM, QJsonValue(totalDataQV.toString()));
    }
  }
  QJsonDocument doc;
  doc.setObject(object);
  QFile config(m_configPath + "config" + whichConn + ".json");
  config.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
  config.write(doc.toJson());
  config.close();
}

void Networkdaemon::trigIndicator(const double totalWFData, const double totalSIMData) {
  QJsonObject objectWF;
  QJsonObject objectSIM;
  QString tdyWF = "today_data_WIFI";
  QString ttlWF = "total_data_WIFI";
  QString tdySIM = "today_data_SIM";
  QString ttlSIM = "total_data_SIM";
  QVariant totalWFDataQV = roundNMB(totalWFData);
  QVariant totalSIMDataQV = roundNMB(totalSIMData);
  objectWF.insert(tdyWF, QJsonValue("0"));
  objectWF.insert(ttlWF, QJsonValue(totalWFDataQV.toString()));
  QJsonDocument docWF;
  docWF.setObject(objectWF);
  QFile configWF(m_configPath + "configWIFI.json");
  configWF.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
  configWF.write(docWF.toJson());
  configWF.close();
  objectSIM.insert(tdySIM, QJsonValue("0"));
  objectSIM.insert(ttlSIM, QJsonValue(totalSIMDataQV.toString()));
  QJsonDocument docSIM;
  docSIM.setObject(objectSIM);
  QFile configSIM(m_configPath + "configSIM.json");
  configSIM.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
  configSIM.write(docSIM.toJson());
  configSIM.close();
}

double Networkdaemon::roundNMB(float var)
{
    float value = (int)(var * 10 + .5);
    return (double)value / 10;
}

QList<QString> Networkdaemon::traslationNotif() {
  QList<QString> strings;
  QFile configFile(m_configPath + "translationNotif.json");
  configFile.open(QFile::ReadOnly | QFile::Text | QFile::Truncate);
  QTextStream in(&configFile);
  int count;
  count = 1;
  while (!in.atEnd()) {
      QString line = in.readLine();
      if (count>1 & count<5) {
        QStringList fields = line.split("<");
        line = fields[1];
        strings << line;
      }
      count = count + 1;
  }
  configFile.close();
  return strings;
}
