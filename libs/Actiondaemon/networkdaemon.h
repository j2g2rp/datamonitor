#ifndef NETWORKDAEMON_H
#define NETWORKDAEMON_H

#include <QNetworkConfigurationManager>

class Networkdaemon {

public:

    QList<QVariant> recBytes();
    quint64 recBytesFromCMD();
    void shootNotification(const QVariant connType, const QVariant totBytes);
    void resetNotification(const QVariant connType);
    void saveDataIndicator(const QString whichConn, const double todayData, const double totalData);
    void trigIndicator(const double totalWFData, const double totalSIMData);
    QList<QString> traslationNotif();

  private:
    QString m_configPath = "/home/phablet/.config/datamonitor.matteobellei/";
    double roundNMB(float var);

};

#endif
