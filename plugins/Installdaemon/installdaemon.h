#ifndef INSTALLDAEMON_H
#define INSTALLDAEMON_H

#include <QObject>
#include <QProcess>

class Installdaemon: public QObject {
    Q_OBJECT

    Q_PROPERTY(bool isInstalled MEMBER m_isInstalled NOTIFY isInstalledChanged)
    Q_PROPERTY(bool isIndInstalled MEMBER m_isIndInstalled NOTIFY isIndicatorInstalledChanged)

public:
    Installdaemon();
    ~Installdaemon() = default;

    Q_INVOKABLE void install();
    Q_INVOKABLE void uninstall();
    Q_INVOKABLE void installInd();
    Q_INVOKABLE void uninstallInd();

    Q_INVOKABLE void storeTimeStep(const QString tstep);

    Q_INVOKABLE void traslationNotif(const QString string1, const QString string2, const QString string3);

Q_SIGNALS:
    void installed(bool success);
    void uninstalled(bool success);
    void installedIndicator(bool success);
    void uninstalledIndicator(bool success);

    void isInstalledChanged(const bool isInstalled);
    void isIndicatorInstalledChanged(const bool isIndInstalled);

private Q_SLOTS:
    void onInstallFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onUninstallFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onInstallIndFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onUninstallIndFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:

    bool checkInstalled();
    bool checkIndicatorInstalled();

    QProcess m_installProcess;
    QProcess m_uninstallProcess;
    QProcess m_stopDaemon;
    QProcess m_installIndProcess;
    QProcess m_uninstallIndProcess;
    bool m_isInstalled = false;
    bool m_isIndInstalled = false;
    QString m_configPath = "/home/phablet/.config/datamonitor.matteobellei/";
};


#endif
