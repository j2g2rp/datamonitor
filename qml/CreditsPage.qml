import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: creditsPage

    header: PageHeader {
        title: i18n.tr("About - Credits")

        StyleHints	{
          foregroundColor: fontColor
          backgroundColor: bkgColor
          dividerColor:	lineColor
        }
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    ScrollView {
        id: scrollView
        anchors {
            top: creditsPage.header.bottom
            topMargin: units.gu(2)
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        Column {
            id: creditsColumn
            spacing: units.gu(2)
            width: scrollView.width

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Thanks to anyone who contributed or just supported the present app. I wish to mention those people who I believe have contributed the most:")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "- Michele (@mymike00): " + i18n.tr("he is the author of the current graphical look of the app. He mostly worked on the .qml side of the code. Thank you Michele.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "- Jonatan Zeidler (@jonny): " + i18n.tr("he is one of the most important contributors to 'clickable' and he also tinkered the .qml code of the present app to permit it to compile for the arm64 architecture in an agnostic and automatic way. Thank you Jonnie.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "- Pgcor (@Pgcor): " + i18n.tr("a big thanks to @Pgcor from the UBports italian channel, because the development of the present app was brought further even when I got no device to test modifications. He successfully tested several app versions on behalf of me, without tiredness.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "- Constantin (@C0n57an71n): " + i18n.tr("thanks to him, dataMonitor has got a new fancy icon. He has a very good artistic touch and helped me a lot to improving my original idea of a suitable icon starting from the gist of my app. Thank you Constantin.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: "- Brian Douglass (@bhdouglass): " + i18n.tr("he is the creator of 'clickable', the most simple and powerful tool to be soon productive to developing for the Ubuntu Touch platform. A big thanks goes to Brian as well.")
                color: fontColor
                textSize: Label.Medium
            }


        }
    }
}
