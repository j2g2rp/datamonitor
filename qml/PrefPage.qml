import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Installdaemon 1.0
import Ubuntu.Components.Pickers 1.3
import QtQuick.Layouts 1.3

Page {
    id: prefPage

    property int numberColors: 9
    property int paletteSize: (scrollView.width/numberColors<100) ? scrollView.width/numberColors : 100
    property int maxVertFontSize: 30
    property int maxHorizFontSize: 34
    property real minOpc: 0.05
    property string color1: Qt.rgba(0,0,0)                      //black
    property string color2: Qt.rgba(0,0,1)                      //blue
    property string color3: Qt.rgba(0.376471,0.376471,0.376471) //grey
    property string color4: Qt.rgba(0,0.6,0)                    //green
    property string color5: Qt.rgba(1,0,0)                      //red
    property string color6: Qt.rgba(1,0,1)                      //fuchsia
    property string color7: Qt.rgba(0,1,1)                      //skyblue
    property string color8: Qt.rgba(1,1,0.2)                    //yellow
    property string color9: Qt.rgba(1,1,1)                      //white
    property string colorTracker: Qt.rgba(1,0.5,0)
    property string borderColorTracker: Qt.rgba(0,0,0.9)

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")

        StyleHints	{
            foregroundColor: fontColor
            backgroundColor: bkgColor
            dividerColor: lineColor
        }

    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    Component.onCompleted: {
        switcherThemes.checkedChanged()
        if (Installdaemon.isIndInstalled) {
            switcherSIMdataToday.enabled = true
            switcherSIMdataTotal.enabled = true
            switcherWIFIdataToday.enabled = true
            switcherWIFIdataTotal.enabled = true
        } else {
            switcherSIMdataToday.enabled = false
            switcherSIMdataTotal.enabled = false
            switcherWIFIdataToday.enabled = false
            switcherWIFIdataTotal.enabled = false
        }
    }

    ScrollView {
        id: scrollView
        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        clip: true

        Column {
            width: scrollView.width
            ListItem {
                divider.colorFrom: lineColor
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableThemesColorStyle
                    title {
                        text: i18n.tr("Enable global Theme style from OS general settings")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherThemes
                        checked: settings.themeSelection
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settings.themeSelection = switcherThemes.checked
                            themeColumn.visible = !checked
                            enableThemeColorDayTime.visible = checked
                            root.themeColorSelection()
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: enableThemeColorDayTime.visible ? enableThemeColorDayTime.height + divider.height : 0
                clip: true
                ListItemLayout {
                    id: enableThemeColorDayTime
                    height: Math.max(mainSlot.height, enableThemeColorDayTimeColumn.height) + padding.top + padding.bottom
                    title {
                        text: i18n.tr("Enable global Theme to follow the day light (SuruDark or Ambiance themes) by setting the time for the switch")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Column {
                        id:enableThemeColorDayTimeColumn
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        spacing: units.gu(1)
                        Switch {
                            id: switcherTimeTheme
                            checked: settings.themeSwitchByTime
                            anchors.horizontalCenter: parent.horizontalCenter
                            onCheckedChanged: {
                                targetTimeSelectorButton.enabled = checked
                                settings.themeSwitchByTime=switcherTimeTheme.checked
                                root.themeColorSelection()
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: dayMornLabel.height + divider.height
                ListItemLayout {
                    id: dayMornLabel
                    title {
                        text: i18n.tr("Daily switch (from 'SuruDark' to 'Ambiance')")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 5
                        color: fontColor
                    }
                    Button {
                        id: targetTimeSelectorButtonMorning
                        color: buttonColor
                        text: settings.daylightSwitchMorning
                        anchors.verticalCenter: parent.verticalCenter
                        enabled:  switcherTimeTheme.checked
                        onClicked: {
                            if (settings.daylightSwitchDateMorning.getHours() != settings.daylightSwitchMorning.substring(0,2))
                            settings.daylightSwitchDateMorning.setHours(settings.daylightSwitchMorning.substring(0,2))
                            PopupUtils.open(popoverTargetTimePickerMorning, targetTimeSelectorButtonMorning)
                        }
                    }
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: dayNightLabel.height + divider.height
                ListItemLayout {
                    id: dayNightLabel
                    title {
                        text: i18n.tr("Nightly switch (from 'Ambiance' to 'SuruDark')")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 5
                        color: fontColor
                    }
                    Button {
                        id: targetTimeSelectorButton
                        color: buttonColor
                        text: settings.daylightSwitch
                        anchors.verticalCenter: parent.verticalCenter
                        enabled:  switcherTimeTheme.checked
                        onClicked: {
                            if (settings.daylightSwitchDate.getHours() != settings.daylightSwitch.substring(0,2))
                            settings.daylightSwitchDate.setHours(settings.daylightSwitch.substring(0,2))
                            PopupUtils.open(popoverTargetTimePicker, targetTimeSelectorButton)
                        }
                    }
                }
            }
            Component {
                id: popoverTargetTimePicker
                Popover {
                    id: popoverTimePicker
                    property alias date: timePicker.date

                    DatePicker {
                        id: timePicker
                        mode: "Hours|Minutes"
                        date: settings.daylightSwitchDate
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            targetTimeSelectorButton.text = Qt.formatDateTime(timePicker.date, "hh mm")
                            settings.daylightSwitchDate=timePicker.date
                            settings.daylightSwitch=targetTimeSelectorButton.text
                            root.themeColorSelection()
                        }
                    }
                }
            }

            Component {
                id: popoverTargetTimePickerMorning
                Popover {
                    id: popoverTimePickerMorning
                    property alias date: timePickerMorning.date

                    DatePicker {
                        id: timePickerMorning
                        mode: "Hours|Minutes"
                        date: settings.daylightSwitchDateMorning
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            targetTimeSelectorButtonMorning.text = Qt.formatDateTime(timePickerMorning.date, "hh mm")
                            settings.daylightSwitchDateMorning=timePickerMorning.date
                            settings.daylightSwitchMorning=targetTimeSelectorButtonMorning.text
                            root.themeColorSelection()
                        }
                    }
                }
            }

            Column {
                id: themeColumn
                width: parent.width
                ListItem {
                    divider.colorFrom: lineColor
                    height: labelPalettes.height + divider.height
                    ListItemLayout {
                        id: labelPalettes
                        title {
                            color: fontColor
                            font.weight: Font.DemiBold
                            text: i18n.tr("Colors Palettes:")
                            maximumLineCount:2
                        }
                        Icon {
                            id: lockColors
                            name: settings.lockedPalette ? "lock" : "lock-broken"
                            height: units.gu(4)
                            width: height
                            color: fontColor
                            SlotsLayout.position: SlotsLayout.Trailing
                            MouseArea {
                                anchors.fill: parent
                                onClicked: settings.lockedPalette = !settings.lockedPalette
                            }
                        }
                    }
                }
                Repeater {
                    model: [
                    {
                        label: i18n.tr("Background color:"),
                        namePalette: "background"
                    },
                    {
                        label: i18n.tr("Font color:"),
                        namePalette: "font"
                    },
                    {
                        label: i18n.tr("Lines color:"),
                        namePalette: "line"
                    },
                    {
                        label: i18n.tr("Buttons color:"),
                        namePalette: "button"
                    },
                    {
                        label: i18n.tr("Grid color (Graph only):"),
                        namePalette: "grid"
                    },
                    {
                        label: i18n.tr("Bars fill color (Graph only):"),
                        namePalette: "bar"
                    }
                    ]
                    delegate: ListItem {
                        divider.colorFrom: lineColor
                        height: colorPickerLayout.height + divider.height
                        enabled: !settings.lockedPalette
                        opacity: enabled ? 1.0 : 0.6
                        SlotsLayout {
                            id: colorPickerLayout
                            property var firstRepeaterIndex: index
                            mainSlot: ColumnLayout {
                                Label {
                                    color: fontColor
                                    text: modelData.label
                                }
                                Flickable {
                                    Layout.fillWidth: true
                                    height: backgroundPalette.height
                                    contentWidth: backgroundPalette.width
                                    Row {
                                        id: backgroundPalette
                                        spacing: units.dp(10)
                                        Repeater {
                                            id: repeater1
                                            model: root.colors
                                            property int selectedIndex: root.customColorsIndex[colorPickerLayout.firstRepeaterIndex]
                                            property string namePalette: modelData.namePalette
                                            delegate: Item {
                                                height: childrenRect.height
                                                width: childrenRect.width
                                                Rectangle {
                                                    id: colorRect
                                                    width: units.gu(6)
                                                    height: width
                                                    color: modelData
                                                    opacity: sliderBkg.value
                                                    MouseArea {
                                                        anchors.fill: parent
                                                        onClicked: {
                                                            var c = colorRect.color
                                                            var i = colorPickerLayout.firstRepeaterIndex
                                                            root.customColors[i] = Qt.rgba(c.r, c.g, c.b, root.opacArray[i])
                                                            root.updateCustomColors()
                                                            root.customColorsIndex[i] = index
                                                            root.updateCustomColorsIndex()
                                                        }
                                                    }
                                                }
                                                Rectangle {
                                                    id: selectedRect
                                                    visible: repeater1.selectedIndex == index
                                                    anchors.verticalCenter: colorRect.verticalCenter
                                                    anchors.horizontalCenter: colorRect.horizontalCenter
                                                    width: colorRect.width/3
                                                    height: colorRect.width/3
                                                    radius: height/2
                                                    color: prefPage.colorTracker
                                                    border {
                                                        color: prefPage.borderColorTracker
                                                        width: units.dp(3)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                RowLayout {
                                    id: fontOpacity
                                    Layout.fillWidth: true
                                    spacing: units.gu(2)
                                    Label {
                                        color: fontColor
                                        text: i18n.tr("Opacity:")
                                    }
                                    Slider {
                                        id: sliderBkg
                                        Layout.fillWidth: true
                                        SlotsLayout.position: SlotsLayout.Trailing
                                        function formatValue(v) { return v.toFixed(2) }
                                        onValueChanged: {
                                            if (themeColumn.visible) {
                                                var c = root.colors[repeater1.selectedIndex]
                                                root.opacArray[index] = value
                                                root.updateOpac()
                                                root.customColors[index] = Qt.rgba(c.r, c.g, c.b, value)
                                                root.updateCustomColors()
                                            }
                                        }
                                        minimumValue: 0.05
                                        maximumValue: 1
                                        value: root.opacArray[index]
                                        live: true
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: daemonLabel.height + divider.height
                ListItemLayout {
                    id: daemonLabel
                    title {
                        color: fontColor
                        font.weight: Font.DemiBold
                        text: i18n.tr("Daemon Settings:")
                        maximumLineCount:2
                    }
                    Icon {
                        id: lockDaemonTime
                        name: settings.lockedDaemTime ? "lock" : "lock-broken"
                        height: units.gu(4)
                        width: height
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                        MouseArea {
                            anchors.fill: parent
                            onClicked: settings.lockedDaemTime = !settings.lockedDaemTime
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: enableDaemonIdleTime.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableDaemonIdleTime
                    title {
                        color: fontColor
                        text: i18n.tr("Set time step for daemon in idle activity")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 5
                    }
                    Icon {
                        id: warningIcon
                        name: "dialog-warning"
                        height: units.gu(4)
                        width: height
                        SlotsLayout.position: SlotsLayout.Leading
                    }
                    subtitle {
                        text: i18n.tr("The smaller it is, the more it affects battery drain but a big time step may seldom lead to incoherent data storage")
                        color: fontColor
                        opacity: 0.8
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 5
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: availTime.height
                ListItemLayout {
                    Label {
                        id: availTime
                        color: fontColor
                        height: units.gu(4)
                        width: Math.min(implicitWidth, enableDaemonIdleTime.width*0.28)
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: false
                        anchors.verticalCenter: parent.verticalCenter
                        text: settings.labelTime
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: sliderDaemonIdleTime.height
                contentItem.clip: false
                Slider {
                    id: sliderDaemonIdleTime
                    enabled: !settings.lockedDaemTime
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: scrollView.width - units.gu(5)
                    function formatValue(v) {
                        if (v>=60) {
                            var initime = v/60
                            var min = Math.floor(initime)
                            var sec = (initime-min)*60
                            var secRnd = Math.floor(sec)
                            var time = min + "\u00A0min and " + secRnd + "\u00A0sec"
                        } else {
                            var time = v.toFixed(0) + "\u00A0sec"
                        }
                        settings.labelTime = time
                        availTime.text = time
                        return time
                    }
                    onValueChanged: {
                        settings.daemonIdleTime =  Math.round(value)
                        var daemonStep = settings.daemonIdleTime
                        Installdaemon.storeTimeStep(String(daemonStep))
                    }
                    minimumValue: 10
                    maximumValue: 600
                    value: settings.daemonIdleTime
                    live: true
                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: deleteDaemonLabel.height + divider.height
                ListItemLayout {
                    id: deleteDaemonLabel
                    title {
                        text: i18n.tr("Remove daemon to save battery. This action will put offline the monitoring of received data")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Button {
                        id: deleteDaemon
                        text: i18n.tr("Remove daemon")
                        color: theme.palette.normal.negative
                        enabled: Installdaemon.isInstalled
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onClicked: {
                            if (Installdaemon.isInstalled) {
                                message.visible = false;
                                Installdaemon.uninstall();
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: daemonLabel.height + divider.height
                ListItemLayout {
                    id: indTitleLabel
                    title {
                        color: fontColor
                        font.weight: Font.DemiBold
                        text: i18n.tr("Indicator Settings:")
                        maximumLineCount:2
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: indicatorLabel.height + divider.height
                ListItemLayout {
                    id: indicatorLabel
                    title {
                        color: fontColor
                        text: i18n.tr("Install dataMonitor indicator to have the data usage monitoring always at your finger tap")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                    }
                    Button {
                        id: indicatorButton
                        property bool installed: (typeof !Installdaemon.isIndInstalled === "boolean") ? !Installdaemon.isIndInstalled : true
                        text: installed ? i18n.tr("Install Indicator") : i18n.tr("Uninstall Indicator")
                        color: installed ? theme.palette.normal.positive : theme.palette.normal.negative
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onClicked: {
                            messageIndicator.visible = true;
                            if (installed) {
                                Installdaemon.installInd()
                                switcherSIMdataToday.enabled = true
                                switcherSIMdataTotal.enabled = true
                                switcherWIFIdataToday.enabled = true
                                switcherWIFIdataTotal.enabled = true
                            } else {
                                Installdaemon.uninstallInd()
                                switcherSIMdataToday.enabled = false
                                switcherSIMdataTotal.enabled = false
                                switcherWIFIdataToday.enabled = false
                                switcherWIFIdataTotal.enabled = false
                            }
                        }
                    }
                }
            }
            ListItem {
                divider.visible: false
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableSIMdataIndicatorToday
                    title {
                        text: i18n.tr("Display today SIM data usage in the indicator")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherSIMdataToday
                        checked: settingsIndicator.simtoggleToday
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settingsIndicator.simtoggleToday = switcherSIMdataToday.checked
                        }
                    }

                }
            }
            ListItem {
                divider.visible: false
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableSIMdataIndicatorTotal
                    title {
                        text: i18n.tr("Display total SIM data usage in the indicator")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherSIMdataTotal
                        checked: settingsIndicator.simtoggleTotal
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settingsIndicator.simtoggleTotal = switcherSIMdataTotal.checked
                        }
                    }

                }
            }
            ListItem {
                divider.visible: false
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableWIFIdataIndicatorToday
                    title {
                        text: i18n.tr("Display today Wi-Fi data usage in the indicator")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherWIFIdataToday
                        checked: settingsIndicator.wifitoggleToday
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settingsIndicator.wifitoggleToday = switcherWIFIdataToday.checked
                        }
                    }

                }
            }
            ListItem {
                divider.visible: false
                height: enableThemesColorStyle.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: enableWIFIdataIndicatorTotal
                    title {
                        text: i18n.tr("Display total Wi-Fi data usage in the indicator")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        maximumLineCount: 10
                        color: fontColor
                    }
                    Switch {
                        id: switcherWIFIdataTotal
                        checked: settingsIndicator.wifitoggleTotal
                        SlotsLayout.position: SlotsLayout.Trailing
                        SlotsLayout.overrideVerticalPositioning: true
                        anchors.verticalCenter: parent.verticalCenter
                        onCheckedChanged: {
                            settingsIndicator.wifitoggleTotal = switcherWIFIdataTotal.checked
                        }
                    }

                }
            }
            ListItem {
                divider.colorFrom: lineColor
                height: messageIndicator.visible ? messageIndicator.height + divider.height : 0
                ListItemLayout {
                    id: messageIndicator
                    title.wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    title.maximumLineCount: 10
                }
            }
        }
    }
    Connections {
        target: Installdaemon

        onUninstalled: {
            messageIndicator.visible = true;
            if (success) {
                messageIndicator.title.text = i18n.tr("Daemon files removed. DataMonitor is offline. Close and open again the 'dataMonitor' app to get daemon files automatically installed.");
                messageIndicator.title.color = UbuntuColors.green;
            }
            else {
                messageIndicator.title.text = i18n.tr("Failed to uninstall both the daemon files.");
                messageIndicator.title.color = UbuntuColors.red;
            }
            scrollView.flickableItem.contentY = scrollView.flickableItem.contentHeight + messageIndicator.height - scrollView.flickableItem.height + scrollView.flickableItem.contentY
        }
        onInstalledIndicator: {
            messageIndicator.visible = true;
            if (success) {
                messageIndicator.title.text = i18n.tr("Indicator successfully installed.");
                messageIndicator.title.color = UbuntuColors.green;
            }
            else {
                messageIndicator.title.text = i18n.tr("Failed to install indicator.");
                messageIndicator.title.color = UbuntuColors.red;
            }
            scrollView.flickableItem.contentY = scrollView.flickableItem.contentHeight + messageIndicator.height - scrollView.flickableItem.height
        }
        onUninstalledIndicator: {
            messageIndicator.visible = true;
            if (success) {
                messageIndicator.title.text = i18n.tr("Indicator successfully uninstalled.");
                messageIndicator.title.color = UbuntuColors.green;
            }
            else {
                messageIndicator.title.text = i18n.tr("Failed to uninstall indicator.");
                messageIndicator.title.color = UbuntuColors.red;
            }
            print("contentY = " + scrollView.flickableItem.contentY)
            print("contentHeight = " + scrollView.flickableItem.contentHeight)
            print("flickableheight = " + scrollView.flickableItem.height)
            print("height =" + scrollView.height)
            scrollView.flickableItem.contentY = scrollView.flickableItem.contentHeight + messageIndicator.height - scrollView.flickableItem.height
        }
    }

    function colorsBystrings(opacity, color) {
        return Qt.rgba(color.r, color.g, color.b, opacity)
    }
}
