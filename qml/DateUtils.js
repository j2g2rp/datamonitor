
/* Utility to work with dates in Javascript */


/* Format input date to have double digits for day and month (default is one digit in js)
   Example: return date like: YYYY-MM-DD
   eg: 2017-04-28
*/
function formatDateToString(date)
{
   var dd = (date.getDate() < 10 ? '0' : '') + date.getDate();
   var MM = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
   var yyyy = date.getFullYear();

   return (yyyy + "-" + MM + "-" + dd);
}

function firstOfMonth(date) {
  var chars = date.lenght;
  var date = date.slice(2, chars);
  date = "01" + date;
  return date;
}

function getDifferenceInDays(dateFrom, dateTo){

    //Get 1 day in milliseconds
    var one_day = 1000*60*60*24;
    var newdateFrom = new Date(dateFrom);
    var newdateTo = new Date(dateTo);
    var monthin = newdateFrom.getMonth();
    var monthout = newdateTo.getMonth();

    var date1_ms = newdateFrom.getTime();
    var date2_ms = newdateTo.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms/one_day);
}


/* Add the provided amount of days at the input date, if amount is negative, subtract them.
   The returned data has pattern YYYY-MM-DD
 */
function addDaysAndFormat(date, days) {

        var date = new Date(date);
        var newDate = new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate() + days,
            0,
            0,
            0,
            0
        );

    return formatDateToString(newDate);
}
