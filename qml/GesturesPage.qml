import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: creditsPage

    header: PageHeader {
        title: i18n.tr("About - Gestures and hidden functions")

        StyleHints	{
          foregroundColor: fontColor
          backgroundColor: bkgColor
          dividerColor:	lineColor
        }
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    ScrollView {
        id: scrollView
        anchors {
            top: creditsPage.header.bottom
            topMargin: units.gu(2)
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        Column {
            id: creditsColumn
            spacing: units.gu(2)
            width: scrollView.width

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("There are some gestures and hidden functions in the app useful to interact with the graph. Let's introduce them.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Gestures (only graph):")
                color: fontColor
                textSize: Label.Large
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Drag downward/upward with two fingers pressed on graph: user can respectively enlarge/narrow the data range.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Drag rightward/leftward with one finger pressed on graph: user can respectively scroll backward/forward into the received data of the next/earlier month.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Keep one finger pressed on graph: a data threshold is generated on the graph. A list of thresholds is immediately visible soon after generation by dragging up the 'BottomEdge' handle.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Touch with one finger a graph bar (fast click): a message bubble is displayed with data recap for the concerned day.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("Hidden functions (only graph):")
                color: fontColor
                textSize: Label.Large
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Threshold list shown in the 'BottomEdge' - THRESHOLD DELETION: for every row corresponding to a single generated threshold, user can delete a threshold by pressing on the selected row and dragging towards right to show the deletion option and then he can press the relevant icon to perform the chosen action.")
                color: fontColor
                textSize: Label.Medium
            }

            Label {
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                text: i18n.tr("- Threshold list shown in the 'BottomEdge' - THRESHOLD MUTING/UNMUTING: for every row corresponding to a single generated threshold, user can mute a threshold by pressing on the selected row and dragging towards left to show the muting/unmuting option and then pressing the relevant icon to perform the chosen action. The muting is kept indefinitely in time and it is reversible following the same procedure.")
                color: fontColor
                textSize: Label.Medium
            }


        }
    }
}
