import QtQuick 2.4
import Ubuntu.Components 1.3
// Credits for the present MessageBubble.qml content go to Kugi Eusebio @javacookies and his Talaan app, from where I
// took inspiration for a message bubble implementation. I found it is a brilliant solution and wanted to employ
// the same idea for my app. Thank you Kugi!
Item {
    id: mainContainer
    property string message
    property double fontSize
    property color bgColor: UbuntuColors.warmGrey
    property int showDuration: 5000
    property alias notifyLoaderItem: notifyLoader.item
    property alias internalAnchors: mainContainer.anchors
    property int startPosition: x
    property real heightLimit: units.gu(30)
    property int endPosition: x+10
    property int bubbleWidth: units.gu(30)
    property bool isShown

    height: units.gu(10)


    function showNotification(txtMessage, txtColor) {
        if (showDuration !== 0) {
            durationTimer.restart()
        }
        notifyLoader.sourceComponent = undefined
        message = (txtMessage === "") ? message : txtMessage
        bgColor = txtColor
        notifyLoader.sourceComponent = notificationUI

        /*Workaround on the issue of TextArea.autosize not properly working on loaders*/
        notifyLoaderItem.messageLabel.text = message
        mainContainer.height = notifyLoader.item.frameShape.height
    }
    function hideNotification() {
        //only execute if the notification bubble is visible
        if (isShown) {
            notifyLoaderItem.closingAnimation.running = true
            durationTimer.stop()
            isShown = false
        }
    }

    onIsShownChanged: {
        if (isShown) {
            notifyLoaderItem.messageLabel.forceActiveFocus()
            notifyLoaderItem.messageLabel.cursorVisible = false
        }
    }

    Loader {
        id: notifyLoader
        anchors.fill: parent
        onLoaded: {
            if (showDuration !== 0) {
                durationTimer.start()
            }
        }
    }
    Timer {
        id: durationTimer
        interval: showDuration
        onTriggered: {
            notifyLoaderItem.closingAnimation.running = true
        }
    }

    Component {
        id: notificationUI
        Item {
            id: notificationItem
            property alias closingAnimation: closingAnimation
            property alias messageLabel: messageLabel
            property alias frameShape: frameShape
            property bool isOpen: false
            property bool isOpening: false
            property bool isClosing: false
            property bool textFocused: false

            UbuntuNumberAnimation on x {
                id: openingAnimation
                from: startPosition
                to: endPosition
                duration: UbuntuAnimation.BriskDuration
                easing: UbuntuAnimation.StandardEasing
                onRunningChanged: {
                    if (!running) {
                        isShown = true
                    }
                }
            }

            UbuntuNumberAnimation on opacity {
                id: closingAnimation
                from: 1
                to: 0
                duration: UbuntuAnimation.SlowDuration
                easing: UbuntuAnimation.StandardEasing
                running: false

                onRunningChanged: {
                    if (!running) {
                        isOpen = false
                        parent.sourceComponent = undefined
                    } else {
                        isClosing = true
                    }
                }
            }

            Rectangle {
                id: frameShape
                color: bgColor === "" ? "black" : bgColor
                anchors.top: parent.top
                anchors.left: parent.left
                radius: 30
                height: {
                    if (heightLimit > 0) {
                        if (messageLabel.height > heightLimit) {
                            heightLimit
                        } else {
                            messageLabel.height
                        }
                    } else {
                        messageLabel.height
                    }
                }
                width: bubbleWidth
            }
            MouseArea {
                id: mouseComment
                anchors.fill: frameShape
                onClicked: hideNotification()
            }

            ScrollView {
                anchors {
                    fill: frameShape
                }

                Flickable {
                    id: messageFlickable
                    boundsBehavior: Flickable.DragAndOvershootBounds
                    interactive: true
                    contentHeight: messageLabel.contentHeight + units.gu(2)

                    TextArea {
                        id: messageLabel
                        font.pixelSize: fontSize
                        anchors {
                            left: parent.left
                            right: parent.right
                            top: parent.top
                        }
                        autoSize: true
                        readOnly: true
                        maximumLineCount: 0
                        verticalAlignment: TextEdit.AlignVCenter
                        wrapMode: TextEdit.WordWrap

                        /*Workaround for bug when autoSize is true, the line count is 1 and the font is too big.
                                            text is clipped horizontally*/
                        Component.onCompleted: {
                            text = "'\n"
                            text = ""
                        }

                        onActiveFocusChanged: {
                            if (!activeFocus && isShown) {
                                hideNotification()
                            }
                        }
                        onSelectedTextChanged: {
                            if (selectedText.length > 0) {
                                cursorVisible = true
                            }
                        }
                    }
                }
            }
        }
    }
}
