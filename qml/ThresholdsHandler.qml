import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import "StorageThresholds.js" as StorageThresholds

Item {

  function getSize(connType)
	{
      StorageThresholds.createTables(connType);
			var numStrings = StorageThresholds.getNumData(connType);
			return numStrings;
	}

	function getValue(index, connType)
	{
			var mbData = StorageThresholds.getValueFromPos(index, connType);
			return mbData;
	}

	function getSilFlag(index, connType)
	{
			var ismute = StorageThresholds.getMuteFromPos(index, connType);
			return ismute;
	}

	function getNotifFlag(index, connType)
	{
			var isNotif = StorageThresholds.getNotifFlagFromPos(index, connType);
			return isNotif;
	}

	function updateNotifFlag(flagNot, value,  connType)
	{
			StorageThresholds.updateNotifFlagValue(flagNot, value, connType);
	}

}
